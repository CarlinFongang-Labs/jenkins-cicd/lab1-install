# Jinkins CICD | Installation

[Please find the specifications by clicking](https://github.com/eazytraining/)

------------

Firstname : Carlin

Surname : FONGANG

Email : fongangcarlin@gmail.com


><img src="https://media.licdn.com/dms/image/C4E03AQEUnPkOFFTrWQ/profile-displayphoto-shrink_400_400/0/1618084678051?e=1710979200&v=beta&t=sMjRKoI0WFlbqYYgN0TWVobs9k31DBeSiOffAOM8HAo" width="50" height="50" alt="Carlin Fongang"> 
>
>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://githut.com/carlinfongang)


_____

## "What is Jenkins?"
Jenkins is a widely used continuous integration and continuous deployment tool in software development. It automates the steps of software compilation, testing, and deployment, thus facilitating the development process.
>![alt text](img/image-0.png)

## Overview of lab
In this lab, we will perform a deployment of a Docker container based on the img/image jenkins from eazytraining.
This jenkins img/image will be run on a single container for testing purposes.


## "Prerequisites" 
1. Install git 
2. Install Docker [Installation script here](https://gitlab.com/CarlinFongang-Labs/docker-labs/docker-install)
3. Install Docker-compose 


## 1. Preparing the environment
 ### 1.1 Setting up the ec2 on AWS
We will proceed with the creation of an instance on Amazon Web Service's EC2.
1. Log in/Create an account and navigate to the EC2 service.
>![alt text](img/image-3.png)

2. Set up the security group to allow inbound and outbound traffic (not recommended in production environment).
EC2 > Security Groups > Inbound rules > Edit Inbound rules
>![alt text](img/image-4.png)

3. Launch an instance of at least t3.medium type.
EC2 > Instances > Launch Instances
spécification 
- Ubuntu server 22.04 LTS
- Generate a key pair for SSH connection.
- Select the security group configured in the previous step 1.1.
- Add the Docker installation script in the "User Data" section under Advanced details. [click here : script installation docker](https://gitlab.com/CarlinFongang-Labs/docker-labs/docker-install#on-ubuntu)
>![alt text](img/image-5.png)
- Once the installation is done, add the current user to the docker group.
`sudo usermod -aG docker $USER && systemctl start docker && systemctl enable docker`

4. Connect via SSH to the EC2 instance.
- Open the terminal and navigate to the directory where the key pair is saved, then enter :
- `ssh -i "key_pair.pem" ubuntu@PUBLIC_IP_ADDRESS_EC2`

 ### 1.2. "Creating a working directory"
 `mkdir jenkins`
 `cd jenkins`
>![alt text](img/image-7.png)

 ### 1.3. "Downloading the docker-compose file"
 curl -O https://raw.githubusercontent.com/eazytrainingfr/jenkins-training/master/docker-compose.yml
 >![alt text](img/image-8.png)

## 2. Install Jenkins with docker-compose
 ### 2.1. Description
The provided code is a YAML configuration for Docker Compose, deploying a Jenkins service. It specifies the use of the Docker image "eazytraining/jenkins" and exposes ports 8080, 443, and 50000 for web interface, secure connection, and Jenkins node communication. The service is configured to automatically restart in case of failure and has access to host system privileges via the "privileged" parameter. It utilizes volumes to store Jenkins data, enabling persistence of configurations and plugins. This file simplifies the deployment and management of a Jenkins instance with a pre-established configuration.
>![alt text](img/image-6.png)

 ### 2.2. Launching Docker Compose to install the Jenkins container.
 `docker compose up -d`
>![alt text](img/image.png)

 ### 2.3. Checking the running containers.
 `docker ps`
>![alt text](img/image-1.png)

 ### 2.4. Retrieving the Jenkins login password.
`docker exec -it jenkins-jenkins-1 cat /var/jenkins_home/secrets/initialAdminPassword`
>![alt text](img/image-2.png)

 ### 2.5. Launch Jenkins.
`http://public_ip_address_ec2:8080`
>![alt text](img/image-9.png)

 ### 2.6. Configuring Jenkins.
>![alt text](img/image-10.png)
*Choosing startup options.*

 ### 2.7. Installing plugins.
 Selection and launching of default plugins.
 >![alt text](img/image-11.png)
 *Save and continuous*

 >![alt text](img/image-13.png)
 *Start*

 ### Creation of the user account.
 >![alt text](img/image-12.png)

### Jenkins user interface
>![alt text](img/image-14.png)
*The user interface of Jenkins*

